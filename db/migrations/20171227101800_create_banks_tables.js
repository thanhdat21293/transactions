exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('banks', (table) => {
        table.increments()
        table.string('name', 30).notNullable() // 'TPBank', 'ACB'
    })

};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('banks')
};