exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('user_loan_item_views', (table) => {
        table.increments()
        table.integer('user_id').notNullable().references('id').inTable('users')
        table.specificType('loan_item_id', 'char(7)').notNullable().references('id').inTable('loan_items')
        table.timestamp('created_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('user_loan_item_views')
};