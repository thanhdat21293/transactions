exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('sales', (table) => {
        table.increments()
        table.string('fullname', 50).notNullable()
        table.specificType('phone', 'char(12)').unique().notNullable()
        table.specificType('email', 'char(80)').unique()
        table.specificType('hash', 'char(32)').notNullable() // Mat khau md5 
        table.specificType('salt', 'char(5)').notNullable()
        table.specificType('avatar', 'char(50)').defaultTo('avatar.png')
        table.enum('position', ['Giám đốc', 'Nhân viên kinh doanh']).defaultTo('Nhân viên kinh doanh')
        table.specificType('location_id', 'char(3)').references('id').inTable('locations')
        table.integer('bank_id').references('id').inTable('banks')
        table.enu('status', ['active', 'pending', 'banned']).notNullable().defaultTo('pending')
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    })

};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('sales')
};