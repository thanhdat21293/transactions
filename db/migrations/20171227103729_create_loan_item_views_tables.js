exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('loan_item_views', (table) => {
        table.specificType('loan_item_id', 'char(7)').notNullable().references('id').inTable('loan_items').primary()
        table.integer('count').defaultTo(0) // Tổng view của loan item
        table.timestamp('created_at').defaultTo(knex.fn.now())
    })

};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('loan_item_views')
};