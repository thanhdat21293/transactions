exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('companies', (table) => {
        table.specificType('id', 'char(10)').unique().notNullable().primary()
        table.string('address', 160).notNullable()
        table.specificType('location_id', 'char(3)').notNullable().references('id').inTable('locations')
        table.timestamp('created_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('companies')
};