exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('loan_requests', (table) => {
        table.increments()
        table.integer('user_id').notNullable().references('id').inTable('users')
        table.specificType('loan_item_id', 'char(7)').notNullable().references('id').inTable('loan_items')
        table.specificType('rate_sale', 'decimal(5, 2) CHECK (rate_sale >= 0 AND rate_sale <= 100)') // Giá trị từ 0.00 den 100.00, VD: 1.23, 66.32
        table.specificType('rate_user', 'decimal(5, 2) CHECK (rate_user >= 0 AND rate_user <= 100)') // Giá trị từ 0.00 den 100.00, VD: 1.23, 66.32
        table.specificType('rate', 'decimal(5, 2) CHECK (rate >= 0 AND rate <= 100)') // Giá trị từ 0.00 den 100.00, VD: 1.23, 66.32
        table.enu('status', ['finish', 'active', 'deal_sale', 'deal_user', 'pending', 'cancel']).notNullable().defaultTo('pending')
        table.timestamp('created_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('loan_requests')
};